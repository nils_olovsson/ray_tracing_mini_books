#include <iostream>
#include <float.h>
#include <random>
#include <exception>

#include <omp.h>

#include "image_2d.h"
#include "vec3.h"
#include "ray.h"
#include "hitable_list.h"
#include "sphere.h"
#include "material.h"
#include "camera.h"

void show_help() {

    printf("Tiny ray tracer from Ray tracing in a weekend\n");
    printf("Example:\n./ray_tracer_1 -ns 50 -nx 200 -ny 100 -f my_image.bmp\n");
    printf("All flags are optional.\n");
    printf("-ns <value>  Set the number of samples per pixel.\n");
    printf("-nx <value>  Set the horizontal image resolution.\n");
    printf("-ny <value>  Set the vertical image resolution.\n");
    printf("-f  <string> Set the output file.\n");
    printf("-simple      Renders a simpler scene.\n");
    printf("-h --help    Displays this message.\n\n");

}

Hitable* random_scene(int n, int& i) {

    Hitable** list = new Hitable*[n+1];
    list[0] = new Sphere(Vec3(0.f,-1000.f,0.f), 1000.f, new Lambertian(Vec3(0.5f,0.5f,0.5f)));
    i=1;

    for(int a=-11; a<11; a++) {
        for(int b=-11; b<11; b++) {
            float choose_mat = random_uniform();
            Vec3 center(a+0.9f*random_uniform(), 0.2f, b+0.9f*random_uniform());

            if( (center-Vec3(4.f,0.2f,0.f)).length() > 0.9f ) {
                if(choose_mat<0.8f) {
                    list[i++] = new Sphere(center, 0.2f,
                                           new Lambertian(Vec3(random_uniform()*random_uniform(),
                                                               random_uniform()*random_uniform(),
                                                               random_uniform()*random_uniform())));
                } else if(choose_mat<0.95f) {
                    list[i++] = new Sphere(center, 0.2f,
                                           new Metal(Vec3(0.5f*(1.f+random_uniform()),
                                                          0.5f*(1.f+random_uniform()),
                                                          0.5f*(1.f+random_uniform())), 0.5f*random_uniform()));

                } else {
                    list[i++] = new Sphere(center, 0.2f, new Dielectric(1.5f));
                }
            }
        }
    }

    list[i++] = new Sphere(Vec3( 0.f,1.f,0.f), 1.f, new Dielectric(1.5f));
    list[i++] = new Sphere(Vec3(-4.f,1.f,0.f), 1.f, new Lambertian(Vec3(0.4f, 0.2f, 0.1f)));
    list[i++] = new Sphere(Vec3( 4.f,1.f,0.f), 1.f, new Metal(Vec3(0.7f,0.6f,0.5f), 0.0f));

    return new HitableList(list, i);
}

Hitable* simple_scene(int& i) {

    Material* material[5];
    material[0] = new Lambertian(Vec3(0.8f,0.3f,0.3f));
    material[1] = new Lambertian(Vec3(0.8f,0.8f,0.0f));
    material[2] = new Metal(Vec3(0.8f,0.6f,0.2f), 0.1f);
    material[3] = new Dielectric(1.5f);
    material[4] = new Dielectric(1.5f);

    Hitable** list = new Hitable*[5];
    list[0] = new Sphere(Vec3( 0.f,0.f,-1.f), 0.5f,      material[0]);
    list[1] = new Sphere(Vec3( 0.f,-100.5f,-1.f), 100.f, material[1]);
    list[2] = new Sphere(Vec3( 1.f,0.f,-1.f), 0.5f,      material[2]);
    list[3] = new Sphere(Vec3(-1.f,0.f,-1.f), 0.5f,      material[3]);
    list[4] = new Sphere(Vec3(-1.f,0.f,-1.f),-0.45f,     material[4]);
   
    i = 5; 
    return new HitableList(list, 5);
}

void clear(HitableList* scene, int n) {
    for(int i=0; i<n; i++) {
        delete scene->list[i]->material_ptr;
        delete scene->list[i];
    }

    delete[] scene->list;

    delete scene;
}

Vec3 color(const Ray& ray, Hitable* world, int depth) {
    HitRecord rec;
    if(world->hit(ray, 0.001f, MAXFLOAT, rec)) {
        Ray scattered;
        Vec3 attenuation;
        if( depth<50 && rec.material_ptr->scatter(ray, rec, attenuation, scattered) ) {
            return attenuation*color(scattered, world, depth+1);
        } else {
            return Vec3(0.f, 0.f, 0.f);
        }
    }
    
    Vec3 unit_dir = unit_vector(ray.direction());
    float t = 0.5f*(unit_dir.y() + 1.0f);
    return (1.0f-t)*Vec3(1.f,1.f,1.f) + t*Vec3(0.5f, 0.7f, 1.0f);
}

int main(int argc, char* argv[]) {

    int ns = 100;
    int nx = 200;
    int ny = 100;
   
    std::string filename = "image.bmp";
    bool help   = false;
    bool simple = false;

    int i=0;
    try {
    while(i<argc) {
        std::string arg = argv[i];
        if(arg=="-ns" && (++i)<argc)
            ns = std::stoi(argv[i]);
        if(arg=="-nx" && (++i)<argc)
            nx = std::stoi(argv[i]);
        if(arg=="-ny" && (++i)<argc)
            ny = std::stoi(argv[i]);
        if(arg=="-simple")
            simple = true;
        if(arg=="-f" && (++i)<argc)
            filename = argv[i];
        if(arg=="-h" || arg=="--help")
            help = true;
        i++;
    }
    } catch(std::invalid_argument e) {
        printf("Invalid argument: %s\n", e.what());
        help = true;
    }

    if(help) {
        show_help();
        return 0;
    }

    printf("Ray tracing %s scene.\n", (simple ? "simple" : "random"));
    printf("Image width:        %d\n", nx);
    printf("Image height:       %d\n", ny);
    printf("Samples per pixel:  %d\n", ns);

    Image2D img = Image2D(ny,nx);

    int nr_used = 0;
    Hitable* world;
    if(!simple) {
        int n = 500;
        world = random_scene(n, nr_used);
    } else {
        world = simple_scene(nr_used);
    }

    Vec3 lookfrom(12.f,1.5f,10.f);
    Vec3 lookat(0.f,0.f,0.f);
    float dist_to_focus = (lookfrom-lookat).length();
    float aperture = 0.1f;
    Camera cam(lookfrom, lookat, Vec3(0.f,1.f,0.f), 20.f, float(nx)/float(ny), aperture, dist_to_focus);

    #pragma omp parallel for
    for(int j=0; j<img.rows(); j++) {

        //int tid = omp_get_thread_num();
        //if (tid == 0 && (j%10)==0) {
        //    int nthreads = omp_get_num_threads();
            //printf("Number of threads = %d\n", nthreads);
        //    printf("Percentage done; %f\n", float(j) / float(img.rows()/nthreads));
        //}

        for(int i=0; i<img.cols(); i++) {

            Vec3 col = Vec3(0.f, 0.f, 0.f);
            for(int s=0; s<ns; s++) {
                float u = ((float)i              + random_uniform()) / (float)img.cols();
                float v = ((float)(img.rows()-j) + random_uniform()) / (float)img.rows();

                Ray ray = cam.getRay(u, v);
                col += color(ray, world, 0);
            }
            col /= (float)ns;

            img(j,i) = col;
        }
    }

    img.gammaCorrection();

    if( img.writeBMP(filename) ) {
        printf("Write image to: %s\n", filename.c_str());
    } else {
        printf("Failed to write image to: %s\n", filename.c_str());
    }
    
    clear( dynamic_cast<HitableList*>(world), nr_used);

    return 0;
}
