
#include <fstream>

#include "image_2d.h"

// =============================================================================
//
// Image2D
//
// =============================================================================

Image2D::Image2D(int rows, int columns) {
    m_rows = rows;
    m_columns = columns;
    allocate();
}

Image2D::Image2D(const Image2D& other) {
    (*this) = other;
}

Image2D::Image2D(Image2D&& other) {
    (*this) = std::move(other);
}

// -----------------------------------------------------------------------------
// Clear, allocate and resize
// -----------------------------------------------------------------------------

void Image2D::clear() {
    if(m_data)
        delete[] m_data;
    m_data = nullptr;
    
    m_columns = 0;
    m_rows    = 0;
}

void Image2D::resize(int rows, int columns) {
    if(rows != m_rows || columns != m_columns) {
        clear();
        m_rows = rows;
        m_columns = columns;
        allocate();
    }
}

void Image2D::allocate() {
    if(m_data) {
        int rows = m_rows;
        int columns = m_columns;
        clear();
        m_rows = rows;
        m_columns = columns;
    }
    // Returns a null pointer instead of throwing if fails to allocate
    //m_data = new (std::nothrow) Vec3[size()];
    m_data = new Vec3[size()];
}

// -----------------------------------------------------------------------------
// Copy and move
// -----------------------------------------------------------------------------

Image2D& Image2D::operator=(const Image2D& other) {
    resize(other.m_rows, m_columns);

    for(int i=0; i<size(); i++)
        m_data[i] = other(i);    

    return *this;
}

Image2D& Image2D::operator=(Image2D&& other) {
    if(this!=&other) {
        std::swap(m_data, other.m_data);
        std::swap(m_rows, other.m_rows);
        std::swap(m_columns, other.m_columns);
    }
    return *this;
}

// -----------------------------------------------------------------------------
// Accessors
// -----------------------------------------------------------------------------

Vec3 Image2D::operator() (int index) const {
    return m_data[index];
}

Vec3& Image2D::operator() (int index) {
    return m_data[index];
}

Vec3 Image2D::operator() (int row, int col) const {
    return m_data[toInd(row, col)];
}

Vec3& Image2D::operator() (int row, int col) {
    return m_data[toInd(row, col)];
}

void Image2D::gammaCorrection() {
    for(int r=0; r<rows(); r++) {
        for(int c=0; c<cols(); c++) {
            (*this)(r,c).e[0] = sqrt((*this)(r,c).e[0]);
            (*this)(r,c).e[1] = sqrt((*this)(r,c).e[1]);
            (*this)(r,c).e[2] = sqrt((*this)(r,c).e[2]);
        }
    }
}

// -----------------------------------------------------------------------------
// File read/write
// -----------------------------------------------------------------------------

bool Image2D::writePPM(const std::string& filename) const {

    std::ofstream file;
    file.open(filename.c_str());
    if(!file.is_open())
        return false;

    file    << "P3\n"
            << "# P3 means colors are in ASCII, then rows, columns and max value\n"
            << cols() << " " << rows() << "\n255\n";

    for(int r=0; r<rows(); r++) {
        for(int c=0; c<cols(); c++) {
            Vec3 color = (*this)(r,c);
            int r = (int) (255.99*color.r());
            int g = (int) (255.99*color.g());
            int b = (int) (255.99*color.b());

            file << r << " " << g << " " << " " << b << "\n";
        }
    }

    file.close();
    return true;
}

bool Image2D::writeBMP(const std::string& filename) const {

    bool success = true;
	int w = cols();
	int h = rows();

    // Create temp. buffer of 8-bit RGB image from original float valued data
    uint8_t* img = new uint8_t[3*size()];
    for(int r=0; r<rows(); r++) {
        for(int c=0; c<cols(); c++) {
            Vec3 color = (*this)(r,c);
            img[(c+r*w)*3+2] = (uint8_t) (255.99*color.r());
            img[(c+r*w)*3+1] = (uint8_t) (255.99*color.g());
            img[(c+r*w)*3+0] = (uint8_t) (255.99*color.b());
        }
    }

    // Set BMP file header values and padding
	int filesize = 54 + 3*w*h;

	unsigned char bmpfileheader[14] = {'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0};
	unsigned char bmpinfoheader[40] = {40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0};
	unsigned char bmppad[3] = {0,0,0};

	bmpfileheader[ 2] = (unsigned char)(filesize    );
	bmpfileheader[ 3] = (unsigned char)(filesize>> 8);
	bmpfileheader[ 4] = (unsigned char)(filesize>>16);
	bmpfileheader[ 5] = (unsigned char)(filesize>>24);

	bmpinfoheader[ 4] = (unsigned char)(       w    );
	bmpinfoheader[ 5] = (unsigned char)(       w>> 8);
	bmpinfoheader[ 6] = (unsigned char)(       w>>16);
	bmpinfoheader[ 7] = (unsigned char)(       w>>24);
	bmpinfoheader[ 8] = (unsigned char)(       h    );
	bmpinfoheader[ 9] = (unsigned char)(       h>> 8);
	bmpinfoheader[10] = (unsigned char)(       h>>16);
	bmpinfoheader[11] = (unsigned char)(       h>>24);

    /* C-style writing
    FILE *f;
	f = fopen(filename.c_str(),"w+b");
	fwrite(bmpfileheader,1,14,f);
	fwrite(bmpinfoheader,1,40,f);
	for(int i=0; i<h; i++) {
		fwrite(img+(w*(h-i-1)*3),3,w,f);
		fwrite(bmppad,1,(4-(w*3)%4)%4,f);
	}
	
    fclose(f);
    */

    // C++ style file writing...
    std::ofstream file;
    file.open(filename.c_str(), std::ofstream::binary);

    if(file.is_open()) {
        file.write(reinterpret_cast<char*>(bmpfileheader), 14);
        file.write(reinterpret_cast<char*>(bmpinfoheader), 40);
        for(int i=0; i<h; i++) {
            file.write( reinterpret_cast<char*>(img+(w*(h-i-1)*3)),3*w);
            file.write( reinterpret_cast<char*>(bmppad), (4-(w*3)%4)%4);
        }
        file.close();
    } else {
        success = false;
    }

    delete[] img;
    return success;
}
