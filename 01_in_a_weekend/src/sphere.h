#ifndef SPHERE_H
#define SPHERE_H

#include "hitable.h"

class Material;

class Sphere : public Hitable {
public:
    Vec3 center;
    float radius;
    //Material* material_ptr;
public:
    Sphere() {}
    Sphere(Vec3 cen, float r, Material* m) {center=cen, radius=r; material_ptr=m;}
    
    virtual bool hit(const Ray& ray, float t_min, float t_max, HitRecord& rec) const;

};

bool Sphere::hit(const Ray& ray, float t_min, float t_max, HitRecord& rec) const {
    Vec3 oc = ray.origin() - center;
    float a = dot(ray.direction(), ray.direction());
    float b = 2.0f * dot(oc, ray.direction());
    float c = dot(oc, oc) - radius*radius;
    float discr = b*b - 4.f*a*c;
    
    if( discr > 0 ) {
        float temp = (-b-sqrt(discr)) / (2.f*a);
        if( temp<t_max && temp>t_min) {
            rec.t = temp;
            rec.p = ray.at(rec.t);
            rec.normal = (rec.p - center) / radius;
            rec.material_ptr = material_ptr;
            return true;
        }
        temp = (-b+sqrt(discr)) / (2.f*a);
        if( temp<t_max && temp>t_min) {
            rec.t = temp;
            rec.p = ray.at(rec.t);
            rec.normal = (rec.p - center) / radius;
            rec.material_ptr = material_ptr;
            return true;
        }
    }
    return false;
}

#endif
