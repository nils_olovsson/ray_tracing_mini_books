#ifndef MATERIAL_H
#define MATERIAL_H

#include <random>

#include "vec3.h"
#include "ray.h"
#include "hitable.h"

std::default_random_engine generator;
std::uniform_real_distribution<float> distribution(0.f, 1.f);

float random_uniform() {
    return distribution(generator);
}

Vec3 random_in_unit_sphere() {
    Vec3 p;
    do {
        p = 2.f*Vec3(random_uniform(), random_uniform(), random_uniform()) - Vec3(1.f,1.f,1.f);
    } while(p.length2() >= 1.f);
    return p;
}

float schlick(float cosine, float ref_idx) {
    float r0 = (1.f-ref_idx) / (1.f+ref_idx);
    r0 = r0*r0;
    return r0 + (1.f-r0)*pow((1.f-cosine),5);
}

class Material {
public:
    virtual bool scatter(const Ray& r_in, const HitRecord& rec, Vec3& attenuation, Ray& scattered) const = 0;
};

class Lambertian : public Material {
public:
    Vec3 albedo;
public:
    Lambertian(const Vec3& a) : albedo(a) {}

    virtual bool scatter(const Ray& r_in, const HitRecord& rec, Vec3& attenuation, Ray& scattered) const {
        Vec3 target = rec.p + rec.normal + random_in_unit_sphere();
        scattered = Ray(rec.p, target-rec.p);
        attenuation = albedo;
        return true;
    }
};

class Metal : public Material {
public:
    Vec3 albedo;
    float fuzz;
public:
    Metal(const Vec3& a, float f) : albedo(a) {fuzz = (f<1.f) ? f :1.f ;}

    virtual bool scatter(const Ray& r_in, const HitRecord& rec, Vec3& attenuation, Ray& scattered) const {
        Vec3 reflected = reflect(unit_vector(r_in.direction()), rec.normal);
        scattered = Ray(rec.p, reflected + fuzz*random_in_unit_sphere());
        attenuation = albedo;
        return (dot(scattered.direction(), rec.normal) > 0);
    }
};

class Dielectric : public Material {
public:
    float ref_idx;

public:
    Dielectric(float ri) : ref_idx(ri) {}
    
    virtual bool scatter(const Ray& r_in, const HitRecord& rec, Vec3& attenuation, Ray& scattered) const {
        Vec3 outward_normal;
        Vec3 reflected = reflect(r_in.direction(), rec.normal);
        float ni_over_nt;
        attenuation =  Vec3(1.f, 1.f, 1.f);
        Vec3 refracted;
        float reflect_prob;
        float cosine;

        if( dot(r_in.direction(), rec.normal)>0.f) {
            outward_normal = -rec.normal;
            ni_over_nt = ref_idx;
            cosine = ref_idx * dot(r_in.direction(), rec.normal) / r_in.direction().length();
        } else {
            outward_normal = rec.normal;
            ni_over_nt = 1.f/ref_idx;
            cosine = -dot(r_in.direction(), rec.normal) / r_in.direction().length();
        }

        if( refract(r_in.direction(), outward_normal, ni_over_nt, refracted) ){
            reflect_prob = schlick(cosine, ref_idx);
        } else {
            scattered = Ray(rec.p, reflected);
            reflect_prob = 1.f;
        }
        
        if( random_uniform() < reflect_prob ) { 
            scattered = Ray(rec.p, reflected);
        } else {
            scattered = Ray(rec.p, refracted);
        }
        return true;
    }
};

#endif
