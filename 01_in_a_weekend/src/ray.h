#ifndef RAY_H
#define RAY_H

#include "vec3.h"

class Ray {
public:
    Vec3 org;
    Vec3 dir;
public:
    Ray() {}
    Ray(const Vec3& o, const Vec3& d) {org=o; dir=d;}
    
    Vec3 origin   () const {return org;}
    Vec3 direction() const {return dir;}

    Vec3 at(float t) const {return org + t*dir;}
    
};

#endif
