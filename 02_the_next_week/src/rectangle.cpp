
#include "rectangle.h"

// =============================================================================
//
// XYRectangle
//
// =============================================================================

bool XYRectangle::hit(const Ray& ray, float t0, float t1, HitRecord& rec) const {
    float t = (m_k-ray.origin().z()) / ray.direction().z();
    if(t<t0 || t>t1)
        return false;

    float x = ray.origin().x() + t*ray.direction().x();
    float y = ray.origin().y() + t*ray.direction().y();

    if(x<m_x0 || x>m_x1 || y<m_y0 || y>m_y1)
        return false;

    rec.u = (x-m_x0) / (m_x1-m_x0);
    rec.v = (y-m_y0) / (m_y1-m_y0);
    rec.t = t;
    
    rec.material_ptr = m_material;
    rec.p            = ray.at(t);
    rec.normal       = Vec3(0,0,1);
    
    return true;
}

bool XYRectangle::bounding_box(float t0, float t1, AABB& box) const {
    box = AABB(Vec3(m_x0, m_y0, m_k-0.0001f), Vec3(m_x1, m_y1, m_k+0.0001f));
    return true;
}

// =============================================================================
//
// XZRectangle
//
// =============================================================================

bool XZRectangle::hit(const Ray& ray, float t0, float t1, HitRecord& rec) const {
    float t = (m_k-ray.origin().y()) / ray.direction().y();
    if(t<t0 || t>t1)
        return false;

    float x = ray.origin().x() + t*ray.direction().x();
    float z = ray.origin().z() + t*ray.direction().z();

    if(x<m_x0 || x>m_x1 || z<m_z0 || z>m_z1)
        return false;

    rec.u = (x-m_x0) / (m_x1-m_x0);
    rec.v = (z-m_z0) / (m_z1-m_z0);
    rec.t = t;
    
    rec.material_ptr = m_material;
    rec.p            = ray.at(t);
    rec.normal       = Vec3(0,1,0);
    
    return true;
}

bool XZRectangle::bounding_box(float t0, float t1, AABB& box) const {
    box = AABB(Vec3(m_x0, m_k-0.0001f, m_z0), Vec3(m_x1, m_k+0.0001f, m_z1));
    return true;
}

// =============================================================================
//
// YZRectangle
//
// =============================================================================

bool YZRectangle::hit(const Ray& ray, float t0, float t1, HitRecord& rec) const {
    float t = (m_k-ray.origin().x()) / ray.direction().x();
    if(t<t0 || t>t1)
        return false;

    float y = ray.origin().y() + t*ray.direction().y();
    float z = ray.origin().z() + t*ray.direction().z();

    if(y<m_y0 || y>m_y1 || z<m_z0 || z>m_z1)
        return false;

    rec.u = (y-m_y0) / (m_y1-m_y0);
    rec.v = (z-m_z0) / (m_z1-m_z0);
    rec.t = t;
    
    rec.material_ptr = m_material;
    rec.p            = ray.at(t);
    rec.normal       = Vec3(1,0,0);
    
    return true;
}

bool YZRectangle::bounding_box(float t0, float t1, AABB& box) const {
    box = AABB(Vec3(m_k-0.0001f, m_y0, m_z0), Vec3(m_k+0.0001f, m_y1, m_z1));
    return true;
}
