
#include "perlin.h"

static float* perlin_generate() {
    float* p = new float[256];
    for(int i=0; i<256; i++)
        p[i] = Random::uniform();
    return p;
}

static Vec3* perlin_generate_vec() {
    Vec3* p = new Vec3[256];
    for(int i=0; i<256; i++) {
        p[i] = Vec3(-1.f + 2.f*Random::uniform(),
                    -1.f + 2.f*Random::uniform(),
                    -1.f + 2.f*Random::uniform());
        //p[i] = Vec3(Random::uniform(),
        //            Random::uniform(),
        //            Random::uniform());
        p[i] = unit_vector(p[i]);
    }
    return p;
}

void permute(int *p, int n) {
    for(int i=n-1; i>0; i--) {
        int target = int(Random::uniform()*(i+1));
        int tmp = p[i];
        p[i] = p[target];
        p[target] = tmp;
    }
}

static int* perlin_generate_perm() {
    int* p = new int[256];
    for(int i=0; i<256; i++)
        p[i] = i;
    permute(p, 256);
    return p;
}

float trilinear_interp(float c[2][2][2], float u, float v, float w) {
    float cum = 0.f;
    for(int i=0; i<2; i++)
        for(int j=0; j<2; j++)
            for(int k=0; k<2; k++)
                cum += (i*u + (1-i)*(1-u))*
                       (j*v + (1-j)*(1-v))*
                       (k*w + (1-k)*(1-w))*c[i][j][k];
    return cum;
}

float perlin_interp(Vec3 c[2][2][2], float u, float v, float w) {
    
    float uu = u*u*(3.f-2.f*u);
    float vv = v*v*(3.f-2.f*v);
    float ww = w*w*(3.f-2.f*w);

    float cum = 0.f;
    for(int i=0; i<2; i++)
        for(int j=0; j<2; j++)
            for(int k=0; k<2; k++) {
                Vec3 weight(u-i, v-j, w-k);
                cum += (i*uu + (1-i)*(1.f-uu))*
                       (j*vv + (1-j)*(1.f-vv))*
                       (k*ww + (1-k)*(1.f-ww))*
                       dot(c[i][j][k], weight);
            }
    return cum;
}


// =============================================================================
//
// Perlin
//
// =============================================================================

Vec3* Perlin::s_ranv   = nullptr;
int*  Perlin::s_perm_x = nullptr;
int*  Perlin::s_perm_y = nullptr;
int*  Perlin::s_perm_z = nullptr;

void Perlin::init() {
    s_ranv   = perlin_generate_vec();
    s_perm_x = perlin_generate_perm();
    s_perm_y = perlin_generate_perm();
    s_perm_z = perlin_generate_perm();
}

void Perlin::clear() {
    delete[] s_ranv;
    delete   s_perm_x;
    delete   s_perm_y;
    delete   s_perm_z;
}

/*
float Perlin::noise1(const Vec3& p) const {
    float u = p.x() - floor(p.x());
    float v = p.y() - floor(p.y());
    float w = p.z() - floor(p.z());

    int i = int(4*u) & 255;
    int j = int(4*v) & 255;
    int k = int(4*w) & 255;
    
    return s_ran_float[s_perm_x[i] ^ s_perm_y[j] ^ s_perm_z[k]];
}
*/

float Perlin::noise2(const Vec3& p) const {
    float u = p.x() - floor(p.x());
    float v = p.y() - floor(p.y());
    float w = p.z() - floor(p.z());

    int i = floor(p.x());
    int j = floor(p.y());
    int k = floor(p.z());
   
    Vec3 c[2][2][2];

    for(int di=0; di<2; di++)
        for(int dj=0; dj<2; dj++)
            for(int dk=0; dk<2; dk++)
                c[di][dj][dk] = s_ranv[s_perm_x[(i+di)&255] ^
                                       s_perm_y[(j+dj)&255] ^
                                       s_perm_z[(k+dk)&255]];
    return perlin_interp(c, u, v, w);
}

float Perlin::turb(const Vec3& p, int depth) const {
    float cum = 0.f;
    Vec3 temp = p;
    float weight = 1.f;
    for(int i=0; i<depth; i++) {
        cum += weight*noise(temp);
        weight *= 0.5f;
        temp   *= 2.f;
    }
    return fabs(cum);
}
