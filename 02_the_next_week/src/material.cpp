
#include "material.h"

inline float schlick(float cosine, float ref_idx) {
    float r0 = (1.f-ref_idx) / (1.f+ref_idx);
    r0 = r0*r0;
    return r0 + (1.f-r0)*pow((1.f-cosine),5);
}

bool Lambertian::scatter(const Ray& r_in, const HitRecord& rec, Vec3& attenuation, Ray& scattered) const {
    Vec3 target = rec.p + rec.normal + Random::in_unit_sphere();
    scattered = Ray(rec.p, target-rec.p, r_in.time());
    attenuation = albedo->value(rec.u, rec.v, rec.p);
    return true;
}
    
bool Metal::scatter(const Ray& r_in, const HitRecord& rec, Vec3& attenuation, Ray& scattered) const {
    Vec3 reflected = reflect(unit_vector(r_in.direction()), rec.normal);
    scattered = Ray(rec.p, reflected + fuzz*Random::in_unit_sphere(), r_in.time());
    attenuation = albedo;
    return (dot(scattered.direction(), rec.normal) > 0);
}

bool Dielectric::scatter(const Ray& r_in, const HitRecord& rec, Vec3& attenuation, Ray& scattered) const {
    Vec3 outward_normal;
    Vec3 reflected = reflect(r_in.direction(), rec.normal);
    float ni_over_nt;
    attenuation =  Vec3(1.f, 1.f, 1.f);
    Vec3 refracted;
    float reflect_prob;
    float cosine;

    if( dot(r_in.direction(), rec.normal)>0.f) {
        outward_normal = -rec.normal;
        ni_over_nt = ref_idx;
        cosine = ref_idx * dot(r_in.direction(), rec.normal) / r_in.direction().length();
    } else {
        outward_normal = rec.normal;
        ni_over_nt = 1.f/ref_idx;
        cosine = -dot(r_in.direction(), rec.normal) / r_in.direction().length();
    }

    if( refract(r_in.direction(), outward_normal, ni_over_nt, refracted) ){
        reflect_prob = schlick(cosine, ref_idx);
    } else {
        scattered = Ray(rec.p, reflected, r_in.time());
        reflect_prob = 1.f;
    }
    
    if( Random::uniform() < reflect_prob ) { 
        scattered = Ray(rec.p, reflected, r_in.time());
    } else {
        scattered = Ray(rec.p, refracted, r_in.time());
    }
    return true;
}

bool LightDiffuse::scatter(const Ray& r_in, const HitRecord& rec, Vec3& attenuation, Ray& scattered) const {
    return false;
}

Vec3 LightDiffuse::emitted(float u, float v, const Vec3& p) const {
    return emit->value(u, v, p);
}
