#ifndef SPHERE_H
#define SPHERE_H

#include "aabb.h"
#include "hitable.h"

class Material;

class Sphere : public Hitable {
public:
    Vec3 center;
    float radius;
    //Material* material_ptr;
public:
    Sphere() {}
    Sphere(Vec3 cen, float r, Material* m) {center=cen, radius=r; material_ptr=m;}
    
    virtual bool hit(const Ray& ray, float t_min, float t_max, HitRecord& rec) const;
    virtual bool bounding_box(float t0, float t1, AABB& box) const;

};

class MovingSphere : public Hitable {
public:
    Vec3 center0, center1;
    float time0, time1;
    float radius;

    //Material* material_ptr;
public:
    MovingSphere() {}
    MovingSphere(Vec3 cen0, Vec3 cen1,
                 float t0, float t1,
                 float r, Material* m) : center0(cen0), center1(cen1), time0(t0), time1(t1), radius(r) {
        material_ptr = m;
    };
    
    virtual bool hit(const Ray& ray, float t_min, float t_max, HitRecord& rec) const;
    virtual bool bounding_box(float t0, float t1, AABB& box) const;
    Vec3 center(float time) const;

};

#endif
