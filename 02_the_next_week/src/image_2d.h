#ifndef IMAGE_2D_H
#define IMAGE_2D_H

#include <string>

#include "vec3.h"

/* ------------------------------------------------------------------------*//**
 * class Image2D
 * \brief   NxM 2D image.
 * \details Image layout: 
 *          ----------> M
 *          | A B C
 *          | D E F
 *          V N
 * ---------------------------------------------------------------------------*/
class Image2D {
private:
protected:
    int m_columns = 0;
    int m_rows    = 0;
    Vec3* m_data = nullptr;

public:
    Image2D(){}
    Image2D(int rows, int columns);
    Image2D(const Image2D& other);
    Image2D(Image2D&& other);
    ~Image2D() {clear();}

    // Clear, allocate and resize
    void clear();
    void resize(int rows, int columns);
    
    // Dimensions
    int      rows  () const {return m_rows;}
    int      cols  () const {return m_columns;}
    int      width () const {return m_columns;}
    int      height() const {return m_rows;}
    int      size  () const {return m_columns*m_rows;}

    // Copy and move
    Image2D& operator=(const Image2D& other);
    Image2D& operator=(Image2D&& other);

    // Accessors
    const Vec3* data() const {return m_data;}

    Vec3  operator() (int index) const;
    Vec3& operator() (int index);

    Vec3  operator() (int row, int col) const;
    Vec3& operator() (int row, int col);

    void gammaCorrection();

    // File read/write
    bool writePPM(const std::string& filename) const;
    bool writeBMP(const std::string& filename) const;
    
    bool readBMP(const std::string& filename);
    
private:
    void allocate();
    
    int toInd(int row, int col) const {
        return row*width() + col;
    }

    void indexToRowCol(int index, int& row, int& col) const {
        row = index / width();
        col = index - row*width();
    }
};

#endif
