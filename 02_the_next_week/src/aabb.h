

#ifndef AABB_H
#define AABB_H

#include "vec3.h"
#include "ray.h"

inline float ffmin(float a, float b) {return a<b ? a : b;}
inline float ffmax(float a, float b) {return a>b ? a : b;}

class AABB {
public:
    Vec3 m_min;
    Vec3 m_max;

public:
    AABB() {}
    AABB(const Vec3& a, const Vec3& b) {
        m_min = a;
        m_max = b;
    }

    Vec3 min() const {return m_min;}
    Vec3 max() const {return m_max;}

    bool hit(const Ray& r, float tmin, float tmax) const;
};

inline bool AABB::hit(const Ray& r, float tmin, float tmax) const {
   
    for(int a=0; a<3; a++) {
        float invD = 1.0f / r.direction()[a];
        float t0 = (min()[a] - r.origin()[a]) * invD;
        float t1 = (max()[a] - r.origin()[a]) * invD;
        if(invD < 0.f)
            std::swap(t0, t1);
        tmin = t0>tmin ? t0 : tmin;
        tmax = t1<tmax ? t1 : tmax;
        if(tmax <= tmin)
            return false;
    }
    return true;
}

inline AABB surrounding_box(const AABB& box0, const AABB& box1) {
    Vec3 small( ffmin(box0.min().x(), box1.min().x()),
                ffmin(box0.min().y(), box1.min().y()),
                ffmin(box0.min().z(), box1.min().z()));

    Vec3 big(   ffmax(box0.max().x(), box1.max().x()),
                ffmax(box0.max().y(), box1.max().y()),
                ffmax(box0.max().z(), box1.max().z()));
    return AABB(small, big);
}

#endif
