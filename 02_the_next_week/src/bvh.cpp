
#include <stdlib.h>

#include "bvh.h"

int box_x_compare(const void* a, const void* b) {
    AABB box_left, box_right;

    Hitable* ah = *(Hitable**)a;
    Hitable* bh = *(Hitable**)b;

    if( !ah->bounding_box(0, 0, box_left) ||
        !bh->bounding_box(0, 0, box_right))
        std::cerr << "error: Not bounding box in BvhNode constructor\n";

    if( (box_left.min().x() - box_right.min().x()) < 0.f )
        return -1;
    else
        return 1;
}

int box_y_compare(const void* a, const void* b) {
    AABB box_left, box_right;

    Hitable* ah = *(Hitable**)a;
    Hitable* bh = *(Hitable**)b;

    if( !ah->bounding_box(0, 0, box_left) ||
        !bh->bounding_box(0, 0, box_right))
        std::cerr << "error: Not bounding box in BvhNode constructor\n";

    if( (box_left.min().y() - box_right.min().y()) < 0.f )
        return -1;
    else
        return 1;
}

int box_z_compare(const void* a, const void* b) {
    AABB box_left, box_right;

    Hitable* ah = *(Hitable**)a;
    Hitable* bh = *(Hitable**)b;

    if( !ah->bounding_box(0, 0, box_left) ||
        !bh->bounding_box(0, 0, box_right))
        std::cerr << "error: Not bounding box in BvhNode constructor\n";

    if( (box_left.min().z() - box_right.min().z()) < 0.f )
        return -1;
    else
        return 1;
}

// =============================================================================
//
// BvhNode
//
// =============================================================================

BvhNode::BvhNode(Hitable** l, int n, float time0, float time1) {
    int axis = int(3*drand48());

    if(axis==0) {
        qsort(l, n, sizeof(Hitable*), box_x_compare);
    } else if(axis==1) {
        qsort(l, n, sizeof(Hitable*), box_y_compare);
    } else {
        qsort(l, n, sizeof(Hitable*), box_z_compare);
    }

    if(n==1) {
        m_left = m_right = l[0];
    } else if(n==2) {
        m_left  = l[0];
        m_right = l[1];
    } else {
        m_left  = new BvhNode(l,     n/2,   time0, time1);
        m_right = new BvhNode(l+n/2, n-n/2, time0, time1);
    }

    AABB box_left, box_right;

    if( !m_left->bounding_box(time0, time1, box_left) ||
        !m_right->bounding_box(time0, time1, box_right) ){
        std::cerr << "error: Not bounding box in BvhNode constructor\n";
    }

    m_box = surrounding_box(box_left, box_right);
}

bool BvhNode::hit(const Ray& r, float t_min, float t_max, HitRecord& rec) const {
    if(m_box.hit(r, t_min, t_max)) {
        if(m_left->hit(r, t_min, t_max, rec)) {
            m_right->hit(r, t_min, rec.t, rec);
            return true;
        } else {
            return m_right->hit(r, t_min, t_max, rec);
        }
    
    }
    return false;
}

/*
bool BvhNode::hit(const Ray& r, float t_min, float t_max, HitRecord& rec) const {

    if( m_box.hit(r, t_min, t_max) ) {

        HitRecord left_rec, right_rec;
        bool hit_left  = m_left->hit (r, t_min, t_max, left_rec);
        bool hit_right = m_right->hit(r, t_min, t_max, right_rec);

        if( hit_left && hit_right) {
            if( left_rec.t < right_rec.t)
                rec = left_rec;
            else
                rec = right_rec;
            return true;
        } else if(hit_left) {
            rec = left_rec;
            return true;
        } else if(hit_right) {
            rec = right_rec;
            return true;
        } else {
            return false;
        }

    } else {
        return false;
    }
}
*/

bool BvhNode::bounding_box(float t0, float t1, AABB& box) const {
    box = m_box;
    return true;
}
