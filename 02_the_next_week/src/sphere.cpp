
#include "sphere.h"

void get_sphere_uv(const Vec3& p, float& u, float& v) {
    float phi = atan2(p.z(), p.x());
    float tht = asin(p.y());
    u = 1.f - (phi+M_PI) / (2.f*M_PI);
    v = (tht + M_PI/2) / M_PI;
}

// =============================================================================
//
// Sphere
//
// =============================================================================

bool Sphere::hit(const Ray& ray, float t_min, float t_max, HitRecord& rec) const {
    Vec3 oc = ray.origin() - center;
    float a = dot(ray.direction(), ray.direction());
    float b = dot(oc, ray.direction());
    float c = dot(oc, oc) - radius*radius;
    float discr = b*b - a*c;
    
    if( discr > 0 ) {
        float temp = (-b-sqrt(discr)) / a;
        if( temp<t_max && temp>t_min) {
            rec.t = temp;
            rec.p = ray.at(rec.t);
            rec.normal = (rec.p - center) / radius;
            rec.material_ptr = material_ptr;
            get_sphere_uv(rec.normal, rec.u, rec.v);
            return true;
        }
        temp = (-b+sqrt(discr)) / a;
        if( temp<t_max && temp>t_min) {
            rec.t = temp;
            rec.p = ray.at(rec.t);
            rec.normal = (rec.p - center) / radius;
            rec.material_ptr = material_ptr;
            get_sphere_uv(rec.normal, rec.u, rec.v);
            return true;
        }
    }
    return false;
}

bool Sphere::bounding_box(float t0, float t1, AABB& box) const {
    box = AABB(center - Vec3(radius, radius, radius), center + Vec3(radius, radius, radius));
    return true;
}

// =============================================================================
//
// MovingSphere
//
// =============================================================================

Vec3 MovingSphere::center(float time) const {
    return center0 + ((time-time0) / (time1-time0)) * (center1-center0);
}

bool MovingSphere::hit(const Ray& ray, float t_min, float t_max, HitRecord& rec) const {
    Vec3 oc = ray.origin() - center(ray.time());
    float a = dot(ray.direction(), ray.direction());
    float b = 2.0f * dot(oc, ray.direction());
    float c = dot(oc, oc) - radius*radius;
    float discr = b*b - 4.f*a*c;
    
    if( discr > 0 ) {
        float temp = (-b-sqrt(discr)) / (2.f*a);
        if( temp<t_max && temp>t_min) {
            rec.t = temp;
            rec.p = ray.at(rec.t);
            rec.normal = (rec.p - center(ray.time())) / radius;
            rec.material_ptr = material_ptr;
            get_sphere_uv(rec.normal, rec.u, rec.v);
            return true;
        }
        temp = (-b+sqrt(discr)) / (2.f*a);
        if( temp<t_max && temp>t_min) {
            rec.t = temp;
            rec.p = ray.at(rec.t);
            rec.normal = (rec.p - center(ray.time())) / radius;
            rec.material_ptr = material_ptr;
            get_sphere_uv(rec.normal, rec.u, rec.v);
            return true;
        }
    }
    return false;
}

bool MovingSphere::bounding_box(float t0, float t1, AABB& box) const {
    AABB box0 = AABB(center0 - Vec3(radius, radius, radius), center0 + Vec3(radius, radius, radius));
    AABB box1 = AABB(center1 - Vec3(radius, radius, radius), center1 + Vec3(radius, radius, radius));
    box = surrounding_box(box0, box1);
    return true;
}
