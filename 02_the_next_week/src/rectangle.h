

#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "aabb.h"
#include "hitable.h"

class XYRectangle: public Hitable {
public:
    Material* m_material;
    float m_x0, m_x1;
    float m_y0, m_y1;
    float m_k;

public:
    
    XYRectangle();
    XYRectangle(float x0, float x1, float y0, float y1, float k, Material* m) :
                m_x0(x0), m_x1(x1), m_y0(y0), m_y1(y1), m_k(k), m_material(m) {};

    virtual bool hit(const Ray& ray, float t_min, float t_max, HitRecord& rec) const;
    virtual bool bounding_box(float t0, float t1, AABB& box) const;
};

class XZRectangle: public Hitable {
public:
    Material* m_material;
    float m_x0, m_x1;
    float m_z0, m_z1;
    float m_k;

public:
    
    XZRectangle();
    XZRectangle(float x0, float x1, float z0, float z1, float k, Material* m) :
                m_x0(x0), m_x1(x1), m_z0(z0), m_z1(z1), m_k(k), m_material(m) {};

    virtual bool hit(const Ray& ray, float t_min, float t_max, HitRecord& rec) const;
    virtual bool bounding_box(float t0, float t1, AABB& box) const;
};

class YZRectangle: public Hitable {
public:
    Material* m_material;
    float m_y0, m_y1;
    float m_z0, m_z1;
    float m_k;

public:
    
    YZRectangle();
    YZRectangle(float y0, float y1, float z0, float z1, float k, Material* m) :
                m_y0(y0), m_y1(y1), m_z0(z0), m_z1(z1), m_k(k), m_material(m) {};

    virtual bool hit(const Ray& ray, float t_min, float t_max, HitRecord& rec) const;
    virtual bool bounding_box(float t0, float t1, AABB& box) const;
};

#endif
