
#ifndef CONSTANT_MEDIUM_H
#define CONSTANT_MEDIUM_H

#include "hitable.h"
#include "material.h"

class ConstantMedium : public Hitable {
public:
    Hitable*  m_boundary;
    Material* m_phase_function;
    float     m_density;

public:
    ConstantMedium(Hitable* b, float d, Texture* p) : m_boundary(b), m_density(d) {
        m_phase_function = new Isotropic(p);
    }

    virtual bool hit(const Ray& ray, float t_min, float t_max, HitRecord& rec) const;
    virtual bool bounding_box(float t0, float t1, AABB& box) const {
        return m_boundary->bounding_box(t0, t1, box);
    }
};

#endif
