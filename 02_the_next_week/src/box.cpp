
#include "box.h"

#include "hitable_list.h"
#include "material.h"
#include "rectangle.h"
    
Box::Box(const Vec3& p0, const Vec3& p1, Material* m) {

    m_p0 = p0;
    m_p1 = p1;

    Hitable** list = new Hitable*[6];

    list[0] =                 new XYRectangle(p0.x(), p1.x(), p0.y(), p1.y(), p1.z(), m);
    list[1] = new FlipNormals(new XYRectangle(p0.x(), p1.x(), p0.y(), p1.y(), p0.z(), m));
    
    list[2] =                 new XZRectangle(p0.x(), p1.x(), p0.z(), p1.z(), p1.y(), m);
    list[3] = new FlipNormals(new XZRectangle(p0.x(), p1.x(), p0.z(), p1.z(), p0.y(), m));
    
    list[4] =                 new YZRectangle(p0.y(), p1.y(), p0.z(), p1.z(), p1.x(), m);
    list[5] = new FlipNormals(new YZRectangle(p0.y(), p1.y(), p0.z(), p1.z(), p0.x(), m));
    
    m_list_ptr = new HitableList(list, 6);
}

Box::~Box() {

}

bool Box::hit(const Ray& ray, float t0, float t1, HitRecord& rec) const {
    return m_list_ptr->hit(ray, t0, t1, rec);
}

bool Box::bounding_box(float t0, float t1, AABB& box) const {
    box = AABB(m_p0, m_p1);
    return true;
}
