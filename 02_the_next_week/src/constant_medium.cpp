
#include <float.h>
#include <math.h>

#include "rt_random.h"

#include "constant_medium.h"

bool ConstantMedium::hit(const Ray& r, float t_min, float t_max, HitRecord& rec) const {

    //bool db = false

    HitRecord rec1, rec2;

    float theta = 0.0001;

    if(m_boundary->hit(r, -FLT_MAX, FLT_MAX, rec1)) {
    if(m_boundary->hit(r, rec1.t+theta, FLT_MAX, rec2)) {

        if(rec1.t < t_min) {rec1.t=t_min;}
        if(rec2.t > t_max) {rec2.t=t_max;}

        if(rec1.t>=rec2.t)
            return false;

        if(rec1.t<0)
            rec1.t=0;

        float dist_in_boundary = (rec2.t-rec1.t)*r.direction().length();
        float hit_dist = -(1/m_density) * log(Random::uniform());

        if(hit_dist<dist_in_boundary) {
            rec.t = rec1.t + hit_dist/r.direction().length();
            rec.p = r.at(rec.t);
            rec.normal = Vec3(1,0,0); // arbitrary
            rec.material_ptr = m_phase_function;
            return true;
        }
    }
    }

    return false;
}
