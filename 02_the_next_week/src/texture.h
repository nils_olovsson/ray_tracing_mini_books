
#ifndef TEXTURE_H
#define TEXTURE_H

#include "vec3.h"
#include "image_2d.h"
#include "perlin.h"

class Texture {
public:
    virtual Vec3 value(float u, float v, const Vec3& p) const = 0;
};

class ConstantTexture : public Texture {
public:
    Vec3 m_color;

public:
    ConstantTexture() {}
    ConstantTexture(Vec3 c) : m_color(c) {}

    virtual Vec3 value(float u, float v, const Vec3& p) const {
        return m_color;
    }
};

class CheckerTexture : public Texture {
public:
    Texture* m_odd  = nullptr;
    Texture* m_even = nullptr;

public:
    CheckerTexture() {}
    CheckerTexture(Texture* t0, Texture* t1): m_odd(t0), m_even(t1) {}

    virtual Vec3 value(float u, float v, const Vec3& p) const {
        if(!m_odd || !m_even)
            return Vec3(0,0,0);

        float sines = sin(10*p.x())*sin(10*p.y())*sin(10*p.z());
        if(sines<0)
            return m_odd->value(u,v,p);
        else
            return m_even->value(u,v,p);
    }
};

class ImageTexture : public Texture {
public:
    Image2D image;

public:
    ImageTexture(const Image2D& img): image(img) {}

    virtual Vec3 value(float u, float v, const Vec3& p) const {
        int i = (  u)*image.width();
        int j = (1-v)*image.height() - 0.001f;
        
        if(i<0) {i=0;}
        if(j<0) {j=0;}
        if(i>image.width()-1)  {i=image.width()-1;}
        if(j>image.height()-1) {j=image.height()-1;}

        Vec3 color = image(j, i);
        return color;
    }
};

class NoiseTexture : public Texture {
public:
    Perlin noise;
    float scale=20.f;

public:
    NoiseTexture() {}
    NoiseTexture(float sc) : scale(sc) {}

    virtual Vec3 value(float u, float v, const Vec3& p) const {
        //return Vec3(1,1,1) * noise.noise(scale*p);
        //Vec3 color = Vec3(1,1,1) * 0.5f * (1.f + sin(0.5f*scale*p.y() + 0.5f*scale*p.z() + 10.f*noise.turb(p)) );
        Vec3 color = Vec3(1,1,1) * 0.5f * (1.f + sin(0.5f*scale*p.y() + 0.1*scale*p.x() + 0.2*scale*p.z() + 2.f*noise.noise(p)) );

        return color;
    }
};

#endif
