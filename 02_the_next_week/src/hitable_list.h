#ifndef HITABLE_LIST_H
#define HITABLE_LIST_H

#include "hitable.h"

class HitableList: public Hitable {
public:
    Hitable** list;
    int list_size;
public:
    HitableList() {};
    HitableList(Hitable** l, int n) {list=l; list_size=n;}

    virtual bool hit(const Ray& ray, float t_min, float t_max, HitRecord& rec) const;
    virtual bool bounding_box(float t0, float t1, AABB& box) const;
};

#endif
