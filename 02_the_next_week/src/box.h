
#ifndef BOX_H
#define BOX_H

#include "aabb.h"
#include "hitable.h"

//class Material;
//class HitableList;

class Box : public Hitable {
public:
    Vec3     m_p0;
    Vec3     m_p1;
    Hitable* m_list_ptr;

public:
    Box() {}
    Box(const Vec3& p0, const Vec3& p1, Material* m);
    ~Box();

    virtual bool hit(const Ray& ray, float t0, float t1, HitRecord& rec) const;
    virtual bool bounding_box(float t0, float t1, AABB& box) const;

};

#endif
