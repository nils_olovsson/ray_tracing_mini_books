/**
 * \file    timer.inl
 * \ref        
 * \author    
 * \date    
 * \brief    
 *
 */

#ifndef BIVI_CORE_TIMER_INL
#define BIVI_CORE_TIMER_INL

/* --------------------------------------------------------------*//**
 * void start()
 * \brief Start the time measurement.
 * \note  To measure accumulative time just use stop continuously
 *        without restarting the Timer using start()
 * -----------------------------------------------------------------*/
template<typename Clock, typename T>
inline void Timer_t<Clock, T>::start() {
    m_start = Clock::now();
} // start()

/* --------------------------------------------------------------*//**
 * void stop()
 * \brief Stop the time measurement.
 * \note  To measure accumulative time just use stop continuously
 *        without restarting the Timer using start()
 * -----------------------------------------------------------------*/
template<typename Clock, typename T>
inline void Timer_t<Clock, T>::stop() {
    time_point time_stop = Clock::now();
    m_time = (std::chrono::duration_cast<duration>(time_stop-m_start)).count();
} // stop()

/* --------------------------------------------------------------*//**
 * T seconds()
 * \brief Return measured time between start and stop in seconds.
 * -----------------------------------------------------------------*/
template<typename Clock, typename T>
inline T Timer_t<Clock, T>::seconds() const {
    return m_time / T(1000);
} // seconds()

/* --------------------------------------------------------------*//**
 * T ms()
 * \brief Return measured time between start and stop in milliseconds.
 * -----------------------------------------------------------------*/
template<typename Clock, typename T>
inline T Timer_t<Clock, T>::ms() const {
    return m_time;
} // ms()

#endif
