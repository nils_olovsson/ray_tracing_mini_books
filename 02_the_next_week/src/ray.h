#ifndef RAY_H
#define RAY_H

#include "vec3.h"

class Ray {
public:
    Vec3 org;
    Vec3 dir;
    float _time;
public:
    Ray() {}
    Ray(const Vec3& o, const Vec3& d, float ti=0.f) {
        org=o; dir=d; _time=ti;
    }
    
    Vec3  origin   () const {return org;}
    Vec3  direction() const {return dir;}
    float time     () const {return _time;}
    Vec3  at(float t) const {return org + t*dir;}
    
};

#endif
