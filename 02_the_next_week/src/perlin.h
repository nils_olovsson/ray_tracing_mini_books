
#ifndef PERLIN_H
#define PERLIN_H

#include "rt_random.h"

class Perlin {
public:
    //float noise1(const Vec3& p) const;
    float noise2(const Vec3& p) const;

    float noise(const Vec3& p) const {return noise2(p);}
    float turb (const Vec3& p, int depth=7) const;

    static void init();
    static void clear();

public:
    static Vec3* s_ranv;
    static int*  s_perm_x;
    static int*  s_perm_y;
    static int*  s_perm_z;

};

#endif
