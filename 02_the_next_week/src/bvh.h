
#ifndef BVH_H
#define BVH_H

#include "aabb.h"
#include "hitable.h"

class BvhNode : public Hitable {
public:
    AABB     m_box;
    Hitable* m_left  = nullptr;
    Hitable* m_right = nullptr;

public:
    BvhNode() {}
    BvhNode(Hitable** l, int n, float time0, float time1);

    ~BvhNode() {
        std::cout << "BvhNode destroy!\n";
        clear();
    }

    virtual bool hit(const Ray& ray, float t_min, float t_max, HitRecord& rec) const;
    virtual bool bounding_box(float t0, float t1, AABB& box) const;

    void clear() {
        if(m_left) {
            BvhNode* node = dynamic_cast<BvhNode*>(m_left);
            if(node)
                node->clear();
            delete m_left;
        }
        
        if(m_right) {
            BvhNode* node = dynamic_cast<BvhNode*>(m_left);
            if(node)
                node->clear();
            delete m_right;
        }
    }
};


#endif
