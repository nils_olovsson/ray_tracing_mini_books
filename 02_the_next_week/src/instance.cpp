
#include <float.h>

#include "instance.h"

#include "hitable_list.h"
#include "material.h"
#include "rectangle.h"

// =============================================================================
//
// Translate
//
// =============================================================================

bool Translate::hit(const Ray& ray, float t_min, float t_max, HitRecord& rec) const {
    Ray mray(ray.origin()-m_offset, ray.direction(), ray.time());
    if( m_ptr->hit(mray, t_min, t_max, rec) ) {
        rec.p += m_offset;
        return true;
    }
    return false;
}

bool Translate::bounding_box(float t0, float t1, AABB& box) const {
    if( m_ptr->bounding_box(t0, t1, box) ) {
        box = AABB(box.min()+m_offset, box.max()+m_offset);
        return true;
    }
    return false;
}

// =============================================================================
//
// RotateY
//
// =============================================================================

RotateY::RotateY(Hitable* p, float angle) {
    m_ptr = p;
    
    float rad = (M_PI/180.f) * angle;
    m_sin_theta = std::sin(rad);
    m_cos_theta = std::cos(rad);

    m_hasbox = m_ptr->bounding_box(0, 1, m_bbox);
    Vec3 box_min( FLT_MAX,  FLT_MAX,  FLT_MAX);
    Vec3 box_max(-FLT_MAX, -FLT_MAX, -FLT_MAX);

    for(int i=0; i<2; i++) {
    for(int j=0; j<2; j++) {
    for(int k=0; k<2; k++) {

        float x = i*m_bbox.max().x() + (1-i)*m_bbox.min().x();
        float y = j*m_bbox.max().y() + (1-j)*m_bbox.min().y();
        float z = k*m_bbox.max().z() + (1-k)*m_bbox.min().z();

        float nx =  m_cos_theta*x + m_sin_theta*z;
        float nz = -m_sin_theta*x + m_cos_theta*z;

        Vec3 t(nx, y, nz);
        for(int c=0; c<3; c++) {
            if(t[c] > box_max[c]) {box_max[c]=t[c];}
            if(t[c] < box_min[c]) {box_min[c]=t[c];}
        }
    }
    }
    }

    std::cout << "box_min: " << box_min << std::endl;
    std::cout << "box_max: " << box_max << std::endl;

    m_bbox = AABB(box_min, box_max);
}

bool RotateY::hit(const Ray& ray, float t_min, float t_max, HitRecord& rec) const {
    Vec3 org = ray.origin();
    Vec3 dir = ray.direction();

    org[0] = m_cos_theta*ray.origin().x() - m_sin_theta*ray.origin().z();
    org[2] = m_sin_theta*ray.origin().x() + m_cos_theta*ray.origin().z();
    
    dir[0] = m_cos_theta*ray.direction().x() - m_sin_theta*ray.direction().z();
    dir[2] = m_sin_theta*ray.direction().x() + m_cos_theta*ray.direction().z();

    Ray rot_r(org, dir, ray.time());

    if( m_ptr->hit(rot_r, t_min, t_max, rec) ) {
        Vec3 p = rec.p;
        Vec3 n = rec.normal;

        p[0] =  m_cos_theta*rec.p.x() + m_sin_theta*rec.p.z();
        p[2] = -m_sin_theta*rec.p.x() + m_cos_theta*rec.p.z();
        
        n[0] =  m_cos_theta*rec.normal.x() + m_sin_theta*rec.normal.z();
        n[2] = -m_sin_theta*rec.normal.x() + m_cos_theta*rec.normal.z();

        rec.p      = p;
        rec.normal = n;

        return true;
    }

    return false;
}
