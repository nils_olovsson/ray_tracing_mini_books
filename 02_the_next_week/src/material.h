#ifndef MATERIAL_H
#define MATERIAL_H


#include "vec3.h"
#include "ray.h"
#include "hitable.h"
#include "rt_random.h"
#include "texture.h"

class Material {
public:
    virtual bool scatter(const Ray&       r_in,
                         const HitRecord& rec,
                               Vec3&      attenuation,
                               Ray&       scattered) const = 0;

    virtual Vec3 emitted(float u, float v, const Vec3& p) const {
        return Vec3(0,0,0);
    }
};

class Lambertian : public Material {
public:
    Texture* albedo;
public:
    Lambertian(Texture* a) : albedo(a) {}

    virtual bool scatter(const Ray& r_in, const HitRecord& rec, Vec3& attenuation, Ray& scattered) const;
};

class Metal : public Material {
public:
    Vec3 albedo;
    float fuzz;
public:
    Metal(const Vec3& a, float f) : albedo(a) {
        fuzz = (f<1.f) ? f :1.f;
    }

    virtual bool scatter(const Ray& r_in, const HitRecord& rec, Vec3& attenuation, Ray& scattered) const;
};

class Dielectric : public Material {
public:
    float ref_idx;

public:
    Dielectric(float ri) : ref_idx(ri) {}
    
    virtual bool scatter(const Ray& r_in, const HitRecord& rec, Vec3& attenuation, Ray& scattered) const;
};

class LightDiffuse : public Material {
public:
    Texture* emit;

public:
    LightDiffuse(Texture* a) : emit(a) {}

    virtual bool scatter(const Ray& r_in, const HitRecord& rec, Vec3& attenuation, Ray& scattered) const;
    virtual Vec3 emitted(float u, float v, const Vec3& p) const;
};

class Isotropic : public Material {
public:
    Texture* m_albedo;

public:
    Isotropic(Texture* a) : m_albedo(a) {}

    virtual bool scatter(const Ray& r_in, const HitRecord& rec, Vec3& attenuation, Ray& scattered) const {
        scattered = Ray(rec.p, Random::in_unit_sphere());
        attenuation = m_albedo->value(rec.u, rec.v, rec.p);
        return true;
    }
};

#endif
