#ifndef HITABLE_H
#define HITABLE_H

#include "ray.h"
#include "aabb.h"

class Material;

struct HitRecord {
    float t;
    float u, v;
    Vec3 p;
    Vec3 normal;
    Material* material_ptr = nullptr;
};

class Hitable {
public:
    Material* material_ptr;
public:
    virtual bool hit(const Ray& ray, float t_min, float t_max, HitRecord& rec) const = 0;
    virtual bool bounding_box(float t0, float t1, AABB& box) const = 0;
};

class FlipNormals: public Hitable {
public:
    Hitable *ptr;
public:
    FlipNormals(Hitable* p) : ptr(p) {}

    virtual bool hit(const Ray& ray, float t_min, float t_max, HitRecord& rec) const {
        if(ptr->hit(ray, t_min, t_max, rec)) {
            rec.normal = -rec.normal;
            return true;
        } else {
            return false;
        }
    }

    virtual bool bounding_box(float t0, float t1, AABB& box) const {
        return ptr->bounding_box(t0, t1, box);
    }
        
};

#endif
