/**
 * \file    timer.h
 * \ref        
 * \author    
 * \date    
 * \brief    
 *
 */

#ifndef BIVI_CORE_TIMER_H
#define BIVI_CORE_TIMER_H

#include <ratio>
#include <chrono>

/* ------------------------------------------------------------------------*//**
 * class Timer
 * \brief A class for time measurements in real time.
 * \note  In most implementations, for x86-x64 PCs, the
 *        high_resolution_clock and steady_clock are the same thing.
 * ---------------------------------------------------------------------------*/
template<typename Clock=std::chrono::high_resolution_clock, typename T=double>
class Timer_t {
private:
    using duration   = std::chrono::duration<T, std::milli>;
    using time_point = std::chrono::time_point<Clock, duration>;
    time_point m_start;
    T          m_time;

public:
    Timer_t() {};
    ~Timer_t(){};

    void start();
    void stop();

    T seconds() const;
    T ms()      const;

}; // template class Timer_t

#include "timer.inl"

using Timer = Timer_t<>;

#endif //BIVI_CORE_TIMER_H
