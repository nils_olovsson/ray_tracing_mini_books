
#ifndef INSTANCE_H
#define INSTANCE_H

#include "aabb.h"
#include "hitable.h"

class Translate : public Hitable {
public:
    Hitable* m_ptr;
    Vec3     m_offset;

public:
    Translate(Hitable* p, const Vec3& tr) : m_ptr(p), m_offset(tr) {}

    virtual bool hit(const Ray& ray, float t_min, float t_max, HitRecord& rec) const;
    virtual bool bounding_box(float t0, float t1, AABB& box) const;
};

class RotateY : public Hitable {
public:
    Hitable* m_ptr;
    float    m_sin_theta;
    float    m_cos_theta;
    bool     m_hasbox;
    AABB     m_bbox;

public:
    RotateY(Hitable* p, float angle);

    virtual bool hit(const Ray& ray, float t_min, float t_max, HitRecord& rec) const;
    virtual bool bounding_box(float t0, float t1, AABB& box) const {
        box = m_bbox; return m_hasbox;
    }
};

#endif
