
#ifndef RT_RANDOM_H
#define RT_RANDOM_H

#include <random>

#include "vec3.h"

class Random {
public:
    static std::default_random_engine            generator;
    static std::uniform_real_distribution<float> distribution;

public:

    static void init();

    static float uniform();
    static Vec3  in_unit_sphere();
    static Vec3  in_unit_disk();

};

#endif
