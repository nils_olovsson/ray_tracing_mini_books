
#include <stdlib.h>

#include "rt_random.h"

std::default_random_engine            Random::generator;
std::uniform_real_distribution<float> Random::distribution;

void Random::init() {

    srand(0);

    std::random_device r;
    std::seed_seq seed{r(), r(), r(), r(), r(), r(), r(), r()};
    generator = std::default_random_engine(seed);

    distribution = std::uniform_real_distribution<float>(0.f, 1.f);
}

float Random::uniform() {
    //return distribution(generator);
    return drand48();
}

Vec3 Random::in_unit_sphere() {
    Vec3 p;
    do {
        p = 2.f*Vec3( uniform(), uniform(), uniform()) - Vec3(1.f,1.f,1.f);
    } while(p.length2() >= 1.f);
    return p;
}

Vec3 Random::in_unit_disk() {
    Vec3 p;
    do {
        p = 2.f*Vec3(uniform(), uniform(), 0.f) - Vec3(1.f,1.f,0.f);
    } while(dot(p,p) >= 1.f);
    return p;
}
