#ifndef VEC3_H
#define VEC3_H

#include <iostream>
#include <math.h>
#include <stdlib.h>

class Vec3 {
public:
    float e[3];

public:
    Vec3() {};
    Vec3(float e0, float e1, float e2) {e[0]=e0; e[1]=e1; e[2]=e2;}

    inline float x() const {return e[0];}
    inline float y() const {return e[1];}
    inline float z() const {return e[2];}
    inline float r() const {return e[0];}
    inline float g() const {return e[1];}
    inline float b() const {return e[2];}

    inline const Vec3&  operator+()       const {return *this;}
    inline       Vec3   operator-()       const {return Vec3(-e[0], -e[1], -e[2]);}
    inline       float  operator[](int i) const {return e[i];}
    inline       float& operator[](int i)       {return e[i];}

    inline Vec3& operator+=(const Vec3& v);
    inline Vec3& operator-=(const Vec3& v);
    inline Vec3& operator*=(const Vec3& v);
    inline Vec3& operator/=(const Vec3& v);
    
    inline Vec3& operator*=(float t);
    inline Vec3& operator/=(float t);
    
    inline float length2() const {
        return e[0]*e[0] + e[1]*e[1] + e[2]*e[2];
    }
    inline float length() const {
        return sqrt(length2());
    }
    inline void makeUnitVector();

    inline bool removeNAN();
    inline bool removeNegative();
    inline void clamp(float l0=0, float l1=1);
};

inline std::istream& operator>>(std::istream& is, Vec3& v) {
    is >> v.e[0] >> v.e[1] >> v.e[2];
    return is;
}

inline std::ostream& operator<<(std::ostream& os, Vec3& v) {
    os << v.e[0] << ", "<< v.e[1] << ", " << v.e[2];
    return os;
}

inline void Vec3::makeUnitVector() {
    float k = 1.f / length();
    e[0] *= k;
    e[1] *= k;
    e[2] *= k;
}

inline Vec3 operator+(const Vec3& v1, const Vec3& v2) {
    return Vec3(v1.x()+v2.x(), v1.y()+v2.y(), v1.z()+v2.z());
}

inline Vec3 operator-(const Vec3& v1, const Vec3& v2) {
    return Vec3(v1.x()-v2.x(), v1.y()-v2.y(), v1.z()-v2.z());
}

inline Vec3 operator*(const Vec3& v1, const Vec3& v2) {
    return Vec3(v1.x()*v2.x(), v1.y()*v2.y(), v1.z()*v2.z());
}

inline Vec3 operator/(const Vec3& v1, const Vec3& v2) {
    return Vec3(v1.x()/v2.x(), v1.y()/v2.y(), v1.z()/v2.z());
}

inline Vec3 operator*(float t, const Vec3& v) {
    return Vec3(t*v.x(), t*v.y(), t*v.z());
}

inline Vec3 operator*(const Vec3& v,  float t) {
    return Vec3(t*v.x(), t*v.y(), t*v.z());
}

inline Vec3 operator/(const Vec3& v,  float t) {
    return Vec3(v.x()/t, v.y()/t, v.z()/t);
}

inline float dot(const Vec3& v1, const Vec3& v2) {
    return v1.x()*v2.x() + v1.y()*v2.y() + v1.z()*v2.z();
}

inline Vec3 cross(const Vec3& v1, const Vec3& v2) {
    return Vec3( v1.y()*v2.z() - v1.z()*v2.y(),
                 v1.z()*v2.x() - v1.x()*v2.z(),
                 v1.x()*v2.y() - v1.y()*v2.x());
}

inline Vec3& Vec3::operator+=(const Vec3& v) {
    e[0] += v.e[0];
    e[1] += v.e[1];
    e[2] += v.e[2];
    return *this;
}

inline Vec3& Vec3::operator-=(const Vec3& v) {
    e[0] -= v.e[0];
    e[1] -= v.e[1];
    e[2] -= v.e[2];
    return *this;
}

inline Vec3& Vec3::operator*=(const Vec3& v) {
    e[0] *= v.e[0];
    e[1] *= v.e[1];
    e[2] *= v.e[2];
    return *this;
}

inline Vec3& Vec3::operator/=(const Vec3& v) {
    e[0] /= v.e[0];
    e[1] /= v.e[1];
    e[2] /= v.e[2];
    return *this;
}

inline Vec3& Vec3::operator*=(float t) {
    e[0] *= t;
    e[1] *= t;
    e[2] *= t;
    return *this;
}

inline Vec3& Vec3::operator/=(float t) {
    e[0] /= t;
    e[1] /= t;
    e[2] /= t;
    return *this;
}

inline Vec3 unit_vector(const Vec3& v) {
    return v / v.length();
}

inline Vec3 reflect(const Vec3& v, const Vec3& n) {
    return v - 2.f*dot(v,n)*n;
}

inline bool refract(const Vec3& v, const Vec3& n, float ni_over_nt, Vec3& refracted) {
    Vec3 uv = unit_vector(v);
    float dt = dot(uv, n);
    float discr = 1.0f - ni_over_nt*ni_over_nt*(1.0f-dt*dt);
    if( discr>0.f ) {
        refracted = ni_over_nt*(uv-n*dt) - n*sqrt(discr);
        return true;
    } else {
        return false;
    }
}

inline bool Vec3::removeNAN() {
    bool b = false;
    if( !(e[0]==e[0]) ) {e[0]=0; b=true;}
    if( !(e[1]==e[1]) ) {e[1]=0; b=true;}
    if( !(e[2]==e[2]) ) {e[2]=0; b=true;}
    return b;
}

inline bool Vec3::removeNegative() {
    bool b = false;
    if( e[0]<0 ) {e[0]=0; b=true;}
    if( e[1]<0 ) {e[1]=0; b=true;}
    if( e[2]<0 ) {e[2]=0; b=true;}
    return b;
}

inline void Vec3::clamp(float l0, float l1) {
         if(e[0]<l0) {e[0]=l0;} 
    else if(e[0]>l1) {e[0]=l1;} 
    
         if(e[1]<l0) {e[1]=l0;} 
    else if(e[1]>l1) {e[1]=l1;} 
    
         if(e[2]<l0) {e[2]=l0;} 
    else if(e[2]>l1) {e[2]=l1;} 
}

#endif
