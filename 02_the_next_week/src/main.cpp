#include <iostream>
#include <float.h>
#include <random>
#include <exception>

#include <omp.h>

#include "rt_random.h"
#include "image_2d.h"
#include "vec3.h"
#include "ray.h"
#include "hitable_list.h"
#include "constant_medium.h"
#include "sphere.h"
#include "rectangle.h"
#include "box.h"
#include "instance.h"
#include "material.h"
#include "camera.h"
#include "bvh.h"
#include "texture.h"
#include "timer.h"

std::vector<Texture*>  textures;
std::vector<Material*> materials;

void show_help() {

    printf("Tiny ray tracer from Ray tracing in a weekend\n");
    printf("Example:\n./ray_tracer_1 -ns 50 -nx 200 -ny 100 -f my_image.bmp\n");
    printf("All flags are optional.\n");
    printf("-ns <value>  Set the number of samples per pixel.\n");
    printf("-nx <value>  Set the horizontal image resolution.\n");
    printf("-ny <value>  Set the vertical image resolution.\n");
    printf("-f  <string> Set the output file.\n");
    printf("-simple      Renders a simpler scene.\n");
    printf("-h --help    Displays this message.\n\n");

}

// -------------------------------------------------------------------
// 
// -------------------------------------------------------------------
Hitable* random_scene(int n, int& i) {

    Hitable** list = new Hitable*[n+1];
    //list[0] = new Sphere(Vec3(0.f,-1000.f,0.f), 1000.f, new Lambertian(Vec3(0.5f,0.5f,0.5f)));

    Texture* ground;
    Texture* odd = new ConstantTexture(Vec3(0.2, 0.3, 0.1)); textures.push_back(odd);
    Texture* evn = new ConstantTexture(Vec3(0.9, 0.9, 0.9)); textures.push_back(evn);
    
    ground = new CheckerTexture(odd, evn);
    //ground = new NoiseTexture(10.f);
    Material* ground_m = new Lambertian(ground);

    textures.push_back(ground);
    materials.push_back(ground_m);

    list[0] = new Sphere(Vec3(0.f,-1000.f,0.f), 1000.f, ground_m );
    i=1;

    Texture*  t;
    Material* m;
    for(int a=-11; a<11; a++) {
        for(int b=-11; b<11; b++) {
            float choose_mat = Random::uniform();
            Vec3 center(a+0.9f*Random::uniform(), 0.2f, b+0.9f*Random::uniform());

            if( (center-Vec3(4.f,0.2f,0.f)).length() > 0.9f ) {
                if(choose_mat<0.8f) {
                    //list[i++] = new MovingSphere(center, (center + Vec3(0.f, 0.5f*Random::uniform(), 0.f)),
                    //                             0.0f, 1.f, 0.2f,

                    t = new ConstantTexture(Vec3(Random::uniform()*Random::uniform(),
                                                 Random::uniform()*Random::uniform(),
                                                 Random::uniform()*Random::uniform()));
                    m = new Lambertian(t);
                    textures.push_back(t);
                    materials.push_back(m);
                    list[i++] = new Sphere(center, 0.2f, m);
                } else if(choose_mat<0.95f) {
                    m = new Metal(Vec3(0.5f*(1.f+Random::uniform()),
                                       0.5f*(1.f+Random::uniform()),
                                       0.5f*(1.f+Random::uniform())), 0.5f*Random::uniform());
                    materials.push_back(m);
                    list[i++] = new Sphere(center, 0.2f, m);
                } else {
                    m = new Dielectric(1.5f);
                    materials.push_back(m);
                    list[i++] = new Sphere(center, 0.2f, m);
                }
            }
        }
    }

    t = new NoiseTexture(); textures.push_back(t);
    //m = new Lambertian(t); materials.push_back(m);
    m = new Lambertian(t);     materials.push_back(m);
    //m = new LightDiffuse(t);     materials.push_back(m);
    list[i++] = new Sphere(Vec3( 0.f,1.f,0.f), 1.f, m);
    //list[i++] = new Sphere(Vec3( 0.f,1.f,0.f), 1.f, new LightDiffuse( new NoiseTexture() ));
    //list[i++] = new Sphere(Vec3(-4.f,1.f,0.f), 1.f, new Dielectric(1.5f));

    t = new ConstantTexture(Vec3(0.4f, 0.2f, 0.1f)); textures.push_back(t);
    m = new Lambertian(t);                           materials.push_back(m);
    list[i++] = new Sphere(Vec3(-4.f,1.f,0.f), 1.f, m );
    //list[i++] = new Sphere(Vec3(-4.f,1.f,0.f), 1.f, new LightDiffuse( new ConstantTexture(Vec3(0.4f, 0.2f, 0.1f)) ));
    //list[i++] = new Sphere(Vec3( 4.f,1.f,0.f), 1.f, new Metal(Vec3(0.7f,0.6f,0.5f), 0.0f));

    Image2D img;
    //if( !img.readBMP("of.bmp") ) {return new BvhNode(list, i, 0.f, 1.f);}
    if( !img.readBMP("of.bmp") ) {return new BvhNode(list, i, 0.f, 1.f);}
    t = new ImageTexture(img);
    //t->image.writeBMP("test_text_written.bmp");
    textures.push_back(t);
    m = new Lambertian(t);     materials.push_back(m);
    //m = new LightDiffuse(t);     materials.push_back(m);
    list[i++] = new Sphere(Vec3( 4.f,1.f,0.f), 1.f, m );

    //return new HitableList(list, i);
    return new BvhNode(list, i, 0.f, 1.f);
}

// -------------------------------------------------------------------
// 
// -------------------------------------------------------------------
Hitable* simple_scene(int& i) {
    /*
    Material* material[5];
    material[0] = new Lambertian(Vec3(0.8f,0.3f,0.3f));
    material[1] = new Lambertian(Vec3(0.8f,0.8f,0.0f));
    material[2] = new Metal(Vec3(0.8f,0.6f,0.2f), 0.1f);
    material[3] = new Dielectric(1.5f);
    material[4] = new Dielectric(1.5f);

    Hitable** list = new Hitable*[5];
    list[0] = new Sphere(Vec3( 0.f,0.f,-1.f), 0.5f,      material[0]);
    list[1] = new Sphere(Vec3( 0.f,-100.5f,-1.f), 100.f, material[1]);
    list[2] = new Sphere(Vec3( 1.f,0.f,-1.f), 0.5f,      material[2]);
    list[3] = new Sphere(Vec3(-1.f,0.f,-1.f), 0.5f,      material[3]);
    list[4] = new Sphere(Vec3(-1.f,0.f,-1.f),-0.45f,     material[4]);
   
    i = 5; 
    return new HitableList(list, 5);
    */
    return nullptr;
}

// -------------------------------------------------------------------
// 
// -------------------------------------------------------------------
Hitable* quad_lit_scene() {
    
    Hitable** list = new Hitable*[4];
    
    Texture* ptext = new NoiseTexture(2);
    Texture* ltext = new ConstantTexture(Vec3(4,4,4));

    Material* lamb  = new Lambertian(ptext);
    Material* light = new LightDiffuse(ltext);

    textures.push_back(ptext);
    textures.push_back(ltext);

    materials.push_back(lamb);
    materials.push_back(light);

    int i = 0;
    list[i++] = new Sphere(Vec3(0,-1000,0), 1000, lamb);
    list[i++] = new Sphere(Vec3(0, 2,   0), 2,    lamb);
//    list[i++] = new Sphere(Vec3(0, 7,   0), 2,    light);
    list[i++] = new XYRectangle(3,5,1,3,-2, light);

    return new BvhNode(list, i, 0.f, 1.f);
}

// -------------------------------------------------------------------
// 
// -------------------------------------------------------------------
Hitable* cornel_box() {
    
    Hitable** list = new Hitable*[8];
    
    // Load image textures and create materials
    Image2D img;
    Texture* mars = nullptr;
    if( !img.readBMP("textures/mars.bmp") ) {
        printf("Texture load failed.\n");
        mars = new ConstantTexture(Vec3(0.65,0.05,0.05));
    } else {
        mars = new ImageTexture(img);
    }
    
    Texture* earth = nullptr;
    if( !img.readBMP("textures/earth.bmp") ) {
        printf("Texture load failed.\n");
        earth = new ConstantTexture(Vec3(0.65,0.05,0.05));
    } else {
        earth = new ImageTexture(img);
    }
   
    textures.push_back(mars);
    textures.push_back(earth);
    
    Material* mat_mars  = new Lambertian(mars);
    Material* mat_earth = new Lambertian(earth);

    materials.push_back(mat_mars);
    materials.push_back(mat_earth);

    // Create textures
    Texture* red   = new ConstantTexture(Vec3(0.65,0.05,0.05));
    Texture* grey = new ConstantTexture(Vec3(0.73,0.73,0.73));
    Texture* white = new ConstantTexture(Vec3(1.0,1.0,1.0));
    Texture* black = new ConstantTexture(Vec3(0.0,0.0,0.0));
    Texture* green = new ConstantTexture(Vec3(0.12,0.45,0.15));
    //Texture* light = new ConstantTexture(Vec3(15.0,15.0,15.0));
    Texture* light = new ConstantTexture(Vec3(3.0,3.0,3.0));
    
    textures.push_back(red);
    textures.push_back(grey);
    textures.push_back(white);
    textures.push_back(black);
    textures.push_back(green);
    textures.push_back(light);

    // Create materials
    Material* mat_red   = new Lambertian(red);
    Material* mat_grey  = new Lambertian(grey);
    Material* mat_white = new Lambertian(white);
    Material* mat_green = new Lambertian(green);
    Material* mat_light = new LightDiffuse(light);

    materials.push_back(mat_red);
    materials.push_back(mat_grey);
    materials.push_back(mat_white);
    materials.push_back(mat_green);
    materials.push_back(mat_light);
    
    Material* metal = new Metal(Vec3(0.5f, 0.5f, 0.5f), 0.9f);
    Material* glass = new Dielectric(1.5f);
    
    materials.push_back(glass);
    materials.push_back(metal);

    // Create geometry
    int i = 0;
    list[i++] = new FlipNormals( new YZRectangle(0, 555, 0, 555, 555, mat_green));
    list[i++] = new YZRectangle(0, 555, 0, 555, 0, mat_red);
    
    //list[i++] = new XZRectangle(213, 343, 227, 332, 554, mat_light);
    list[i++] = new XZRectangle(113, 443, 127, 432, 554, mat_light);

    list[i++] = new FlipNormals(new XZRectangle(0, 555, 0, 555, 555, mat_grey));
    list[i++] = new                 XZRectangle(0, 555, 0, 555,   0, mat_grey);
    list[i++] = new FlipNormals(new XYRectangle(0, 555, 0, 555, 555, mat_grey));
   
    float r = 50.f; 
    list[i++] = new Sphere(Vec3(130+r, 165+r+5, 65+r),  r, glass);
    list[i++] = new Sphere(Vec3(265+r, 330+r+5, 295+r), r, mat_earth);
    //list[i++] = new Sphere(Vec3(200, 100, 200), 50.f, glass);
    
    //list[i++] = new Box(Vec3(130, 0,  65), Vec3(295, 165, 230), mat_white);
    //list[i++] = new Box(Vec3(265, 0, 295), Vec3(430, 330, 460), mat_white);
   
    auto b1 = new Translate(
                    new RotateY(
                        new Box(Vec3(0,0,0), Vec3(165, 165, 165), mat_grey),
                        -18.f),
                    Vec3(130, 0, 65));
    auto b2 = new Translate(
                    new RotateY(
                        new Box(Vec3(0,0,0), Vec3(165, 330, 165), mat_grey),
                        15.f),
                    Vec3(265, 0, 295));

    //list[i++] = new ConstantMedium(b1, 0.01, white);
    //list[i++] = new ConstantMedium(b2, 0.01, black);

    list[i++] = b1;
    list[i++] = b2;

    return new BvhNode(list, i, 0.f, 1.f);
    //return new HitableList(list, i);
}

// -------------------------------------------------------------------
// 
// -------------------------------------------------------------------
Hitable* final_scene() {

    int nb = 20;
    Hitable** list     = new Hitable*[30];
    Hitable** boxlist  = new Hitable*[10000];
    Hitable** boxlist2 = new Hitable*[10000];

    // Initialize image texture and material
    Image2D img;
    Texture* earth_texture = nullptr;
    if( !img.readBMP("textures/earth.bmp") ) {
        printf("Texture load failed.\n");
        earth_texture = new ConstantTexture(Vec3(0.65,0.05,0.05));
    } else {
        earth_texture = new ImageTexture(img);
    }
    textures.push_back(earth_texture);
    Material* earth = new Lambertian(earth_texture);
    materials.push_back(earth);
    
    // Initialize all textures
    Texture* light_texture   = new ConstantTexture(Vec3(7.0,7.0,7.0));
    Texture* ground_texture  = new ConstantTexture(Vec3(0.48, 0.83, 0.53));
    Texture* white_texture   = new ConstantTexture(Vec3(0.73,0.73,0.73));
    Texture* brown_texture   = new ConstantTexture(Vec3(0.7,0.3,0.1));
    Texture* medium1_texture = new ConstantTexture(Vec3(0.2,0.4,0.9));
    Texture* medium2_texture = new ConstantTexture(Vec3(1.0,1.0,1.0));
    Texture* perlin_texture  = new NoiseTexture(0.25);

    textures.push_back(light_texture);
    textures.push_back(ground_texture);
    textures.push_back(white_texture);
    textures.push_back(brown_texture);
    textures.push_back(medium1_texture);
    textures.push_back(medium2_texture);
    textures.push_back(perlin_texture);

    // Initialize all materials
    Material* light  = new LightDiffuse(light_texture);
    Material* ground = new Lambertian(ground_texture);
    Material* white  = new Lambertian(white_texture);
    Material* brown  = new Lambertian(brown_texture);
    Material* perlin = new Lambertian(perlin_texture);
    Material* glass  = new Dielectric(1.5);
    Material* metal  = new Metal(Vec3(0.8, 0.8, 0.9), 10);

    materials.push_back(light);
    materials.push_back(ground);
    materials.push_back(white);
    materials.push_back(brown);
    materials.push_back(perlin);
    materials.push_back(glass);
    materials.push_back(metal);

    // Create the leveled ground
    int b = 0;
    for(int i=0; i<nb; i++) { 
        for(int j=0; j<nb; j++) { 
            float w = 100;
            float x0 = -1000 + i*w;
            float z0 = -1000 + j*w;
            float y0 = 0;
            float x1 = x0 + w;
            float z1 = z0 + w;
            float y1 = 100*(Random::uniform()+0.01);
            boxlist[b++] = new Box(Vec3(x0,y0,z0), Vec3(x1,y1,z1), ground);
        }
    }

    int l = 0;
    list[l++] = new BvhNode(boxlist, b, 0, 1);

    // Add light
    list[l++] = new XZRectangle(123,423,147,412,554,light);
    Vec3 center(400,400,200);
    
    // Add large spheres
    list[l++] = new MovingSphere(center, center+Vec3(30,0,0),0,1,50,brown);
    list[l++] = new Sphere(Vec3(260,150, 45), 50, glass);
    list[l++] = new Sphere(Vec3(0,  150,145), 50, metal);

    Hitable* boundary;
    boundary = new Sphere(Vec3(360,150,145),70,glass);
    list[l++] = new ConstantMedium(boundary, 0.2, medium1_texture);
    boundary = new Sphere(Vec3(0,0,0),5000,glass);
    list[l++] = new ConstantMedium(boundary, 0.0001, medium2_texture);

    list[l++] = new Sphere(Vec3(400,200,400),100,earth);
    list[l++] = new Sphere(Vec3(220,280,300),80, perlin);

    // Add small spheres inside cube
    int ns = 1000;
    for(int j=0; j<ns; j++) {
        boxlist2[j] = new Sphere(Vec3(165*Random::uniform(), 165*Random::uniform(), 165*Random::uniform()), 10, white);
    }
    list[l++] = new Translate(new RotateY(new BvhNode(boxlist2, ns, 0.0, 1.0), 15), Vec3(-100, 270, 395));

    return new HitableList(list, l);
}

// -------------------------------------------------------------------
// 
// -------------------------------------------------------------------
void clear(HitableList* scene, int n) {
    for(int i=0; i<n; i++) {
        delete scene->list[i]->material_ptr;
        delete scene->list[i];
    }

    delete[] scene->list;

    delete scene;
}

// -------------------------------------------------------------------
// color(...)
// -------------------------------------------------------------------
Vec3 color(const Ray& ray, Hitable* world, int depth) {
    HitRecord rec;
    if(world->hit(ray, 0.001f, MAXFLOAT, rec)) {
        Ray scattered;
        Vec3 attenuation;
        Vec3 emitted = rec.material_ptr->emitted(rec.u, rec.v, rec.p);

        if( depth<50 && rec.material_ptr->scatter(ray, rec, attenuation, scattered) ) {
            return emitted + attenuation*color(scattered, world, depth+1);
        } else {
            //return Vec3(0.f, 0.f, 0.f);
            return emitted;
        }
    }
    
    Vec3 unit_dir = unit_vector(ray.direction());
    float t = 0.5f*(unit_dir.y() + 1.0f);
    //return (1.0f-t)*Vec3(1.f,1.f,1.f) + t*Vec3(0.5f, 0.7f, 1.0f);
    //return 0.1f * ((1.0f-t)*Vec3(1.f,1.f,1.f) + t*Vec3(0.5f, 0.7f, 1.0f));
    return Vec3(0,0,0);
}

// -------------------------------------------------------------------
// 
// -------------------------------------------------------------------
Camera create_camera(int type, int nx, int ny) {
    
    Camera camera;
    
    Vec3 lookfrom(22.f,4.0f,10.f);
    Vec3 lookat(0.f,0.f,0.f);
    float dist_to_focus = 10.f;//(lookfrom-lookat).length();
    float aperture = 0.0f;
    float vfov = 20.f;
    
    switch(type) {
    case(0):
        lookfrom = Vec3(22.f,4.0f,10.f);
        lookat   = Vec3(0.f,0.f,0.f);
        dist_to_focus = 10.f;//(lookfrom-lookat).length();
        aperture = 0.0f;
        vfov = 20.f;
        camera = Camera(lookfrom, lookat, Vec3(0.f,1.f,0.f), vfov, float(nx)/float(ny), aperture, dist_to_focus, 0.f, 1.f);
        break;
    case(1):
        lookfrom = Vec3(278, 278, -800);
        lookat   = Vec3(278, 278,0);
        dist_to_focus = 10.f;//(lookfrom-lookat).length();
        aperture = 0.0f;
        vfov = 40.f;
        camera = Camera(lookfrom, lookat, Vec3(0.f,1.f,0.f), vfov, float(nx)/float(ny), aperture, dist_to_focus, 0.f, 1.f);
        break;
    case(2):
        lookfrom = Vec3(400, 175, -600);
        lookat   = Vec3(278, 300,0);
        dist_to_focus = 10.f;//(lookfrom-lookat).length();
        aperture = 0.0f;
        vfov = 40.f;
        camera = Camera(lookfrom, lookat, Vec3(0.f,1.f,0.f), vfov, float(nx)/float(ny), aperture, dist_to_focus, 0.f, 1.f);
        break;
    }

    return camera;
}

// -----------------------------------------------------------------------------
// Main
// -----------------------------------------------------------------------------
int main(int argc, char* argv[]) {
    Random::init();
    Perlin::init();

    int ns = 200;
    int nx = 960;
    int ny = 640;
   
    std::string filename = "image.bmp";
    bool help   = false;
    bool simple = false;

    int i=0;
    try {
    while(i<argc) {
        std::string arg = argv[i];
        if(arg=="-ns" && (++i)<argc)
            ns = std::stoi(argv[i]);
        if(arg=="-nx" && (++i)<argc)
            nx = std::stoi(argv[i]);
        if(arg=="-ny" && (++i)<argc)
            ny = std::stoi(argv[i]);
        if(arg=="-simple")
            simple = true;
        if(arg=="-f" && (++i)<argc)
            filename = argv[i];
        if(arg=="-h" || arg=="--help")
            help = true;
        i++;
    }
    } catch(std::invalid_argument e) {
        printf("Invalid argument: %s\n", e.what());
        help = true;
    }

    if(help) {
        show_help();
        return 0;
    }

    printf("Ray tracing %s scene.\n", (simple ? "simple" : "random"));
    printf("Image width:        %d\n", nx);
    printf("Image height:       %d\n", ny);
    printf("Samples per pixel:  %d\n", ns);

    Image2D img = Image2D(ny,nx);

    int nr_used = 0;
    Hitable* world;
    Camera cam;

    if(!simple) {
        int n = 500;
        //world = random_scene(n, nr_used);
        //world = quad_lit_scene();
        //world = cornel_box();
        world = final_scene();
        cam = create_camera(2, nx, ny);
    } else {
        world = simple_scene(nr_used);
        cam = create_camera(0, nx, ny);
    }


    Timer timer;
    timer.start();

    int counter = 1;

    #pragma omp parallel for
    for(int j=0; j<img.rows(); j++) {

        //int tid = omp_get_thread_num();
        //if (tid == 0 && (j%10)==0) {
        //    int nthreads = omp_get_num_threads();
            //printf("Number of threads = %d\n", nthreads);
        //    printf("Percentage done; %f\n", float(j) / float(img.rows()/nthreads));
        //}
        //if( (j%10)==0 ) {
        //    printf("Row %d of probably %d\n", counter, (1000/12));
        //    counter+=10;
        //}
        for(int i=0; i<img.cols(); i++) {

            Vec3 col = Vec3(0.f, 0.f, 0.f);
            for(int s=0; s<ns; s++) {
                float u = ((float)i              + Random::uniform()) / (float)img.cols();
                float v = ((float)(img.rows()-j) + Random::uniform()) / (float)img.rows();

                Ray ray = cam.getRay(u, v);
                Vec3 c = color(ray, world, 0);
                
                if(c.removeNAN())
                    printf("Removed a nan\n");
                if(c.removeNegative())
                    printf("Removed a negative\n");
                //c.clamp();
                col += c/float(ns);
            }
            //col /= (float)ns;
            col.clamp();
            img(j,i) = col;
        }
    }

    timer.stop();
    printf ("It took me %f seconds.\n", timer.seconds());

    img.gammaCorrection();

    if( img.writeBMP(filename) ) {
        printf("Write image to: %s\n", filename.c_str());
    } else {
        printf("Failed to write image to: %s\n", filename.c_str());
    }
    
    printf("Clear up world\n");
    //clear( dynamic_cast<HitableList*>(world), nr_used);

    //world->clear();
    delete world;

    for(auto& t: textures)
        delete t;
    for(auto& m: materials)
        delete m;

    Perlin::clear();

    return 0;
}
